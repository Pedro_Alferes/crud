import { Component, OnInit } from '@angular/core';
import { Employer } from './employer';
@Component({
  selector: 'app-employers',
  templateUrl: './employers.component.html',
  styleUrls: ['./employers.component.css']
})
export class EmployersComponent implements OnInit {
  employers : Employer[]= [];
  newEmployer : Employer = new Employer('', 0, '');
  constructor() { }
  ngOnInit() {
    this.newEmployer.name = '';
    this.newEmployer.age = 0;
    this.newEmployer.profession = '';
  }
// this function add the employers on my md-list
  addEmployer() {
    if (this.newEmployer.name !== '' || this.newEmployer.age !== 0 || this.newEmployer.profession !== '') {
      this.employers.push(new Employer(this.newEmployer.name, this.newEmployer.age, this.newEmployer.profession));
      this.newEmployer.name = '';
      this.newEmployer.age = 0;
      this.newEmployer.profession = '';
    }
  }
  removeEmployer(obj: Employer){
    this.employers = this.employers.filter(e => e !== obj);
  }
// Todo create the delete Employer




// Todo  Check EditEmployer Status

}
