export class Employer{
    name: String;
    age: number;
    profession: String;
    public constructor(name: String, age: number, profession: String){
        this.name = name;
        this.age = age;
        this.profession = profession;
    }
}